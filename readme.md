# Pre-requisite

## Docker

Make sure you got docker desktop -> https://docs.docker.com/docker-for-windows/install/

## Gradle

You will need to install gradle in order to compile the project -> https://gradle.org/
On windows, don't forget to initialize environment variables

## Intelli-J (optional) or anyother IDE 

Since kotlin was created by JetBrains (IntelliJ's creators), you'd better use this IDE for convenient uses.
It will automatically download last gradle version -> https://www.jetbrains.com/fr-fr/idea/download/

# Project init

## Git 
use cmd and type "https://gitlab.com/Antoine34170/cv-restapi.git" in the sub-folder directory

## Docker-image
in order to get the docker image used you need to get into the directory and type "docker-compose up"
It will download the image in the docker-compose.yml

## Launch project
./gradlew :bootRun or launch via intelliJ button it's going to compile the project

# Tools used
## Swagger
Used to visualize all endpoint and get informations about it (type of var returned ...)
To visualize launch http://localhost:8080/swagger

## Flyway
When you first compile app with gradle, it will create a flyway_database and check if you have VX__xxxxxx.sql file
if it's not the case it will apply sql script into the database.
if you want to make modifications, go into /src/main/ressources and create a script V(n+1)__name_of_your_script.sql
Never modify those script because md5 verification of files will fail the compilation



