package portfolio.restapi.service


import org.springframework.stereotype.Service
import portfolio.restapi.dto.MiscDto
import portfolio.restapi.jpa.MiscRepository
import portfolio.restapi.jpa.toMiscDto

@Service
class MiscService(
    private val miscRepository: MiscRepository
) {
    fun getMisc(): List<MiscDto> {
        val allMiscs = miscRepository.findAll()
        val allMiscsDto = mutableSetOf<MiscDto>()
        allMiscs.forEach {
            allMiscsDto.add(it.toMiscDto())
        }
        return allMiscsDto.toList()
    }
}