package portfolio.restapi.service

import org.springframework.stereotype.Service
import portfolio.restapi.dto.FormationDto
import portfolio.restapi.jpa.FormationRepository
import portfolio.restapi.jpa.toFormationDto


@Service
class FormationService(
    private val formationRepository: FormationRepository
) {
    fun getFormation(): List<FormationDto> {
        val allFormations = formationRepository.findAll()
        val allFormationsDto = mutableSetOf<FormationDto>()
        allFormations.forEach {
            allFormationsDto.add(it.toFormationDto())
        }
        return allFormationsDto.toList()
    }
}