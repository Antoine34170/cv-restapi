package portfolio.restapi.service

import org.springframework.data.domain.Sort
import org.springframework.stereotype.Service
import portfolio.restapi.dto.ExperienceDto
import portfolio.restapi.jpa.ExperienceRepository
import portfolio.restapi.jpa.toExperienceDto


@Service
class ExperienceService(
    private val experienceRepository: ExperienceRepository
) {
    fun getExperience(): List<ExperienceDto> {
        val allExperiences = experienceRepository.findAll(Sort.by("id").ascending())
        val allExperiencesDto = mutableSetOf<ExperienceDto>()
        allExperiences.forEach {
            allExperiencesDto.add(it.toExperienceDto())
        }
        return allExperiencesDto.toList()
    }

}