package portfolio.restapi.service

import org.springframework.stereotype.Service
import portfolio.restapi.dto.LanguageDto
import portfolio.restapi.jpa.LanguageRepository
import portfolio.restapi.jpa.toLanguageDto


@Service
class LanguageService(
    private val languageRepository : LanguageRepository
) {

    fun getLanguage() : List<LanguageDto> {
        val allLanguages = languageRepository.findAll()
        val allLanguagesDto = mutableSetOf<LanguageDto>()
        allLanguages.forEach {
            allLanguagesDto.add(it.toLanguageDto())
        }
        return allLanguagesDto.toList()
    }
}