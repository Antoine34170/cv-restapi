package portfolio.restapi.service

import org.springframework.stereotype.Service
import portfolio.restapi.dto.FrameworkDto
import portfolio.restapi.jpa.FrameworkRepository
import portfolio.restapi.jpa.toFrameworkDto

@Service
class FrameworkService(
    private val frameworkRepository: FrameworkRepository
) {
    fun getFramework(): List<FrameworkDto> {
        val allFrameworks = frameworkRepository.findAll()
        val allFrameworksDto = mutableSetOf<FrameworkDto>()
        allFrameworks.forEach {
            allFrameworksDto.add(it.toFrameworkDto())
        }
        return allFrameworksDto.toList()

    }
}