package portfolio.restapi.service

import org.springframework.stereotype.Service
import portfolio.restapi.dto.DatabaseDto
import portfolio.restapi.jpa.DatabaseRepository
import portfolio.restapi.jpa.toDatabaseDto

@Service
class DatabaseService(
    private val databaseRepository: DatabaseRepository
) {
    fun getDatabase(): List<DatabaseDto> {
        val allDatabases = databaseRepository.findAll()
        val allDatabasesDto = mutableSetOf<DatabaseDto>()
        allDatabases.forEach {
            allDatabasesDto.add(it.toDatabaseDto())
        }
        return allDatabasesDto.toList()
    }
}
