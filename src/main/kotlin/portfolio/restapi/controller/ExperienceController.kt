package portfolio.restapi.controller

import com.elbonpais.gargleaguebackend.controllers.APIController
import io.swagger.annotations.Api
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseStatus
import portfolio.restapi.dto.ExperienceDto
import portfolio.restapi.service.ExperienceService

@Api( description = "Experience Controller")
@CrossOrigin
@APIController
@RequestMapping("/api/experience")
class ExperienceController (
    private val experienceService : ExperienceService,
) {

    @GetMapping()
    @ResponseStatus(HttpStatus.OK)
    fun getExperience() : List<ExperienceDto> {
        return experienceService.getExperience()
    }
}