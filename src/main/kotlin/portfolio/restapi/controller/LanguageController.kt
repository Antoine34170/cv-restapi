package portfolio.restapi.controller

import com.elbonpais.gargleaguebackend.controllers.APIController
import io.swagger.annotations.Api
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseStatus
import portfolio.restapi.dto.LanguageDto
import portfolio.restapi.service.LanguageService


@Api( description = "Language Controller")
@CrossOrigin
@APIController
@RequestMapping("/api/language")
class LanguageController(
    private val languageService : LanguageService
) {
    @GetMapping()
    @ResponseStatus(HttpStatus.OK)
    fun getLanguage() : List<LanguageDto> {
        return languageService.getLanguage()
    }
}