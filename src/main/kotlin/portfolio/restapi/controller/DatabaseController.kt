package portfolio.restapi.controller

import com.elbonpais.gargleaguebackend.controllers.APIController
import io.swagger.annotations.Api
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseStatus
import portfolio.restapi.dto.DatabaseDto
import portfolio.restapi.service.DatabaseService

@Api( description = "Database Controller")
@CrossOrigin
@APIController
@RequestMapping("/api/database")
class DatabaseController (
    private val databaseService : DatabaseService,
    ) {

    @GetMapping()
    @ResponseStatus(HttpStatus.OK)
    fun getDatabase() : List<DatabaseDto> {
        return databaseService.getDatabase()
    }

}