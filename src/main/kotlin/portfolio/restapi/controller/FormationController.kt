package portfolio.restapi.controller

import com.elbonpais.gargleaguebackend.controllers.APIController
import io.swagger.annotations.Api
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseStatus
import portfolio.restapi.dto.FormationDto
import portfolio.restapi.service.FormationService

@Api( description = "Formation Controller")
@CrossOrigin
@APIController
@RequestMapping("/api/formation")
class FormationController (
    private val formationService : FormationService,
) {

    @GetMapping()
    @ResponseStatus(HttpStatus.OK)
    fun getFormation() : List<FormationDto> {
        return formationService.getFormation()
    }

}