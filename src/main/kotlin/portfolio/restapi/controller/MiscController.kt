package portfolio.restapi.controller

import com.elbonpais.gargleaguebackend.controllers.APIController
import io.swagger.annotations.Api
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseStatus
import portfolio.restapi.dto.MiscDto
import portfolio.restapi.service.MiscService

@Api( description = "Misc Controller")
@CrossOrigin
@APIController
@RequestMapping("/api/misc")
class MiscController(
    private val miscService: MiscService
) {
    @GetMapping()
    @ResponseStatus(HttpStatus.OK)
    fun getMisc() : List<MiscDto> {
        return miscService.getMisc()
    }
}