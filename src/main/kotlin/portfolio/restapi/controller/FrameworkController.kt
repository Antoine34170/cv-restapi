package portfolio.restapi.controller

import com.elbonpais.gargleaguebackend.controllers.APIController
import io.swagger.annotations.Api
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseStatus
import portfolio.restapi.dto.FrameworkDto
import portfolio.restapi.service.FrameworkService

@Api( description = "Framework Controller")
@CrossOrigin
@APIController
@RequestMapping("/api/framework")
class FrameworkController(
    private val frameworkService: FrameworkService
) {
    @GetMapping()
    @ResponseStatus(HttpStatus.OK)
    fun getFramework() : List<FrameworkDto>  {
        return frameworkService.getFramework()
    }
}