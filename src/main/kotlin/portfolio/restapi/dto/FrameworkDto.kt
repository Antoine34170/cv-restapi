package portfolio.restapi.dto

data class FrameworkDto(
        val id: Number,
        val name: String,
        val logo:String
)
