package portfolio.restapi.dto

data class LanguageDto(
        val id: Number,
        val name: String,
        val logo:String
)
