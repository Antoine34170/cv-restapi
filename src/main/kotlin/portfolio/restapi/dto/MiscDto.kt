package portfolio.restapi.dto

data class MiscDto(
    val id: Number,
    val name: String,
    val logo:String
)
