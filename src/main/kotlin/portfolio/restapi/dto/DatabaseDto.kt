package portfolio.restapi.dto


data class DatabaseDto (
        val id: Number,
        val name: String,
        val logo:String
)

