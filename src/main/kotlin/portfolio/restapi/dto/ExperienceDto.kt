package portfolio.restapi.dto

import java.util.*

data class ExperienceDto(
        val id: Number,
        val name:String,
        val beginDate:Date,
        val endDate: Date,
        val details:List<String>,
)
