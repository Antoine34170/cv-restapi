package portfolio.restapi.jpa

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface FormationRepository: JpaRepository<FormationEntity, Long> {

    fun findByName(name : String) : FormationEntity

}
