package portfolio.restapi.jpa

import portfolio.restapi.dto.DatabaseDto
import javax.persistence.*


@Entity
@Table(name = "database")
data class DatabaseEntity(
    @Id
    @GeneratedValue
    @Column(name = "id")
    val id: Long,
    @Column(name = "name")
    val name: String,
    @Column(name = "logo")
    var logo: String
    )

fun DatabaseEntity.toDatabaseDto() : DatabaseDto {
    val id = this.id
    val name = this.name
    val logo = this.logo
    return DatabaseDto(id,name,logo);
}
