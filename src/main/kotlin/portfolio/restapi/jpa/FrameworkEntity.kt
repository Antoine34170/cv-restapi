package portfolio.restapi.jpa

import portfolio.restapi.dto.FrameworkDto
import javax.persistence.*


@Entity
@Table(name = "framework")
data class FrameworkEntity(
    @Id
    @GeneratedValue
    @Column(name = "id")
    val id: Long,
    @Column(name = "name")
    val name: String,
    @Column(name = "logo")
    var logo: String
)

fun FrameworkEntity.toFrameworkDto() : FrameworkDto {
    val id = this.id
    val name = this.name
    val logo = this.logo
    return FrameworkDto(id,name,logo);
}