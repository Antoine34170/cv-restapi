package portfolio.restapi.jpa

import javax.persistence.*

@Entity
@Table(name = "experience_details")
data class ExperienceDetailsEntity(
    @Id
    @GeneratedValue
    @Column(name = "id")
    val id: Long,
    @Column(name = "details")
    val details: String,
    @JoinColumn(name="id_experience")
    @ManyToOne(fetch = FetchType.LAZY)
    var experienceEntity : ExperienceEntity
 ) {
}