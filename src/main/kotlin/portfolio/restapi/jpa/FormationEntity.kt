package portfolio.restapi.jpa

import portfolio.restapi.dto.FormationDto
import java.sql.Date
import javax.persistence.*


@Entity
@Table(name = "formation")
data class FormationEntity(
    @Id
    @GeneratedValue
    @Column(name = "id")
    val id: Long,
    @Column(name = "name")
    val name: String,
    @Column(name = "begin_date")
    val beginDate: Date,
    @Column(name = "end_date")
    val endDate: Date,
    @OneToMany(mappedBy = "formationEntity",cascade = [CascadeType.ALL],fetch = FetchType.EAGER)
    var formationDetails: List<FormationDetailsEntity> = mutableListOf()
)

fun FormationEntity.toFormationDto() : FormationDto {
    val id = this.id
    val name = this.name
    val beginDate = this.beginDate
    val endDate = this.endDate
    val details = mutableListOf<String>()
    this.formationDetails.forEach{
        details.add(it.details)
    }
    return FormationDto(id,name,beginDate,endDate,details.toList())
}
