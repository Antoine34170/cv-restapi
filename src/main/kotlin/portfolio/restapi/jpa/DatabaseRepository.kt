package portfolio.restapi.jpa

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface DatabaseRepository  : JpaRepository<DatabaseEntity, Long> {

    fun findByName(name : String) : DatabaseEntity
    fun findByLogo(logo : String) : DatabaseEntity
}