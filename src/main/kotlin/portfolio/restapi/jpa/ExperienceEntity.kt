package portfolio.restapi.jpa

import portfolio.restapi.dto.ExperienceDto
import java.sql.Date
import javax.persistence.*


@Entity
@Table(name = "experience")
data class ExperienceEntity(
    @Id
    @GeneratedValue
    @Column(name = "id")
    val id: Long,
    @Column(name = "name")
    val name: String,
    @Column(name = "begin_date")
    val beginDate: Date,
    @Column(name = "end_date")
    val endDate: Date,
    @OneToMany(mappedBy = "experienceEntity",cascade = [CascadeType.ALL],fetch = FetchType.EAGER)
    var experienceDetails: List<ExperienceDetailsEntity> = mutableListOf()
)

fun ExperienceEntity.toExperienceDto() : ExperienceDto {
    val id = this.id
    val name = this.name
    val beginDate = this.beginDate
    val endDate = this.endDate
    val details = mutableListOf<String>()
    this.experienceDetails.forEach{
        details.add(it.details)
        }
    return ExperienceDto(id,name,beginDate,endDate,details.toList())
}