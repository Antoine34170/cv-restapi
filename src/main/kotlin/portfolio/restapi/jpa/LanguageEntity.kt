package portfolio.restapi.jpa

import portfolio.restapi.dto.LanguageDto
import javax.persistence.*

@Entity
@Table(name="language")
data class LanguageEntity(
    @Id
    @GeneratedValue
    @Column(name = "id")
    val id: Long,
    @Column(name = "name")
    val name: String,
    @Column(name = "logo")
    var logo: String
)

fun LanguageEntity.toLanguageDto() : LanguageDto {
    val id = this.id
    val name = this.name
    val logo = this.logo
    return LanguageDto(id,name,logo);
}
