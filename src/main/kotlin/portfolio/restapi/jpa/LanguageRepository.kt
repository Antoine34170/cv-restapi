package portfolio.restapi.jpa

import org.springframework.data.jpa.repository.JpaRepository

interface LanguageRepository : JpaRepository<LanguageEntity, Long> {
    fun findByName(name : String) : LanguageEntity
    fun findByLogo(logo : String) : LanguageEntity
}