package portfolio.restapi.jpa

import portfolio.restapi.dto.MiscDto
import javax.persistence.*


@Entity
@Table(name = "misc")

data class MiscEntity(
    @Id
    @GeneratedValue
    @Column(name = "id")
    val id: Long,
    @Column(name = "name")
    val name: String,
    @Column(name = "logo")
    var logo: String
)


fun MiscEntity.toMiscDto() : MiscDto {
    val id = this.id
    val name = this.name
    val logo = this.logo
    return MiscDto(id,name,logo);
}
