package portfolio.restapi.jpa

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface ExperienceDetailsRepository : JpaRepository<ExperienceDetailsEntity, Long> {
    fun findByDetails(details: String) : ExperienceDetailsEntity

}