package portfolio.restapi.jpa

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface MiscRepository : JpaRepository<MiscEntity,Long> {
    fun findByName(name : String) : DatabaseEntity
    fun findByLogo(logo : String) : DatabaseEntity
}