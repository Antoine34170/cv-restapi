package portfolio.restapi.jpa

import org.springframework.data.jpa.repository.JpaRepository

interface FrameworkRepository : JpaRepository<FrameworkEntity, Long> {

    fun findByName(name : String) : FrameworkEntity
    fun findByLogo(logo : String) : FrameworkEntity
}