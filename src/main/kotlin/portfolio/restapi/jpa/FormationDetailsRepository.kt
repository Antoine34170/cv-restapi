package portfolio.restapi.jpa

import org.springframework.data.jpa.repository.JpaRepository

interface FormationDetailsRepository : JpaRepository<FormationDetailsEntity, Long> {
    fun findByDetails(details: String) : FormationDetailsEntity
}