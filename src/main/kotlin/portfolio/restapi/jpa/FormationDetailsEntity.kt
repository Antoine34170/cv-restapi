package portfolio.restapi.jpa

import javax.persistence.*

@Entity
@Table(name = "formation_details")
data class FormationDetailsEntity(
    @Id
    @GeneratedValue
    @Column(name = "id")
    val id: Long,
    @Column(name = "details")
    val details: String,
    @JoinColumn(name="id_formation")
    @ManyToOne(fetch = FetchType.LAZY)
    var formationEntity : FormationEntity
) {
}
