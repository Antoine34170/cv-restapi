package portfolio.restapi.jpa

import org.springframework.data.domain.Sort
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface ExperienceRepository: JpaRepository<ExperienceEntity, Long> {

    fun findByName(name : String) : ExperienceEntity

}