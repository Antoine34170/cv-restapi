package portfolio.restapi

import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@EnableAutoConfiguration
@SpringBootApplication
class RestapiApplication

fun main(args: Array<String>) {
	runApplication<RestapiApplication>(*args)
}
