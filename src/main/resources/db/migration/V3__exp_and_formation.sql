CREATE TABLE formation (
id int PRIMARY KEY,
name varchar(50) NOT NULL,
begin_date DATE,
end_date DATE DEFAULT Now(),
details varchar(255)[]
);

CREATE TABLE experience (
id int PRIMARY KEY,
name varchar(50) NOT NULL,
begin_date DATE,
end_date DATE DEFAULT Now(),
details varchar(255)[]
);

insert into formation (id,name,begin_date, end_date,details)
values (1,'Concepteur Développeur','2021-01-25','2021-11-14',ARRAY['Framework MVC N-tiers', 'Conception de bases de données', 'Gestion de projets']),
(2,'Formation Autodidacte','2021-11-01',Now(),ARRAY['Framework : Angular, Spring, Hibernate', 'Langages : Javascript, Typescript, Java, Kotlin', 'Misc : Docker, PGSQL, Git']),
(3,'BTS Informatique de Gestion','2006-07-05','2009-06-30',ARRAY['VLAN', 'routage NAT/PAT', 'DNS', 'DHCP','TCP/IP']);

insert into experience (id,name,begin_date, end_date,details)
values (1, 'Technicien Support N2', '2018-09-01','2020-11-30', ARRAY ['Formation, accompagnement et support sur les outils huissiers dejustice', 'Documentation technique / utilisateur']),
(2, 'Technicien Support', '2018-03-01','2018-09-01', ARRAY ['Formation, accompagnement et support sur les outils notaires', 'Virtualisation (Hyper-V), Messagerie (Exchange), Sauvegarde (ArcServe)']),
(3,'Technicien Retail Itinérant','2017-01-01','2018-02-01',ARRAY ['SAV Matériel Informatique', 'Installation, formation & SAV bornes bancaires']),
(4,'Technicien Support','2015-09-01','2017-01-31',ARRAY ['Formation, accompagnement et support logiciel Backoffice / E-commerce', 'Diverses problématiques réseaux']),
(5,'Technicien Support','2012-06-01','2014-05-31',ARRAY ['Installation, formation et SAV systèmes encaissements à distance/sur site', 'Installation bornes wifi']);
