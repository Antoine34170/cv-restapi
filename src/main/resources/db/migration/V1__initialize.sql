CREATE TABLE framework(
    id int PRIMARY KEY,
    name varchar(35) NOT NULL,
    logo varchar(100) NOT NULL
);

insert into framework (id, name, logo)  values (1,'Angular','angular.png'),(2,'Spring Boot','spring.png'),(3,'Hibernate','hibernate.png')

