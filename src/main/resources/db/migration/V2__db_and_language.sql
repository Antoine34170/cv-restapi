CREATE TABLE database(
    id int PRIMARY KEY,
    name varchar(35) NOT NULL,
    logo varchar(100) NOT NULL
);

insert into database (id, name, logo)  values (1,'PostgreSQL','postgresql.png'),(2,'MySQL','mysql.png');

-- database language ------------------------------
CREATE TABLE language(
    id int PRIMARY KEY,
    name varchar(35) NOT NULL,
    logo varchar(100) NOT NULL
);

insert into language (id, name, logo)  values (1,'Java','java.png'),(2,'Kotlin','kotlin.png'),(3,'Javascript','javascript.png'),(4,'Typescript','typescript.png'),(5,'PHP','php.png');
-- end language ---------------------------------