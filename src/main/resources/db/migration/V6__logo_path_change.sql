-- Instead of mapping result in front or back, i apply this patch in PGSQL

update misc set logo = '../../assets/' || logo;
update database set logo = '../../assets/' || logo;
update language set logo = '../../assets/' || logo;
update framework set logo = '../../assets/' || logo;
