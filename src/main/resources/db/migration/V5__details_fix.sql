ALTER TABLE experience
DROP COLUMN details;

ALTER TABLE formation
DROP COLUMN details;
-- drop those 2 column for jpa fix that need to create another table to map list<String> of details

CREATE TABLE experience_details(
id INT PRIMARY KEY,
details VARCHAR(255),
id_experience INT REFERENCES experience(id)
);

CREATE TABLE formation_details(
id INT PRIMARY KEY,
details VARCHAR(255),
id_formation INT REFERENCES formation(id)
);

-- JEU DE DONNEES

INSERT INTO experience_details (id,details,id_experience)
VALUES (1, 'Formation, accompagnement et support sur les outils huissiers de justice',1),
(2,'Documentation technique / utilisateur',1),
(3,'Formation, accompagnement et support sur les outils notaires',2),
(4,'Virtualisation (Hyper-V), Messagerie (Exchange), Sauvegarde (ArcServe)',2),
(5,'Installation, formation & SAV bornes bancaires',3),
(6,'SAV Matériel Informatique',3),
(7,'Formation, accompagnement et support logiciel Backoffice / E-commerce',4),
(8,'Diverses problématiques réseaux',4),
(9,'Installation, formation et SAV systèmes encaissements à distance/sur site',5);

INSERT INTO formation_details (id,details,id_formation)
VALUES (1, 'Framework MVC N-tiers',1),
(2,'Conception de bases de données',1),
(3,'Gestion de projets',1),
(4,'Framework : Angular, Spring, Hibernate',2),
(5,'Langages : Javascript, Typescript, Java, Kotlin',2),
(6,'Misc : Docker, PGSQL, Git',2),
(7,'VLAN, routage NAT/PAT, DNS, DHCP',3);



--
