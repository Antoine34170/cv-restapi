CREATE TABLE misc(
id INT PRIMARY KEY,
name varchar(50),
logo varchar(50)
);

insert into misc (id,name,logo) values (1, 'Docker', 'docker.png'),
(2, 'Gitlab', 'gitlab.png'),
(3, 'swagger', 'swagger.png'),
(4, 'flyway', 'flyway.png');